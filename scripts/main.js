/**
 * For starting - create car with consumption:
 * var car = Car(5);
          *var car = Car(5,5);
          *var car = Car(5,5,120);
 * Car module. Created car with custom consumption.
 * @param consumption {number}
 * @return {{
 *            start: start,
*             riding: riding,
 *            checkGas: checkGas,
  *           refueling: refueling
*           }}
 * @constructor
 */
function Car(consumption, oil, maxSpeed) {
  if (!isNumeric(consumption)) {
    showMessage('Wrong consumption', 'error');
    return;
  }

  let gasBalance = 100;
  let oilBalance = 100;
  let gasConsumption = consumption / 100;
  let oilConsumption = oil / 10000
  let gasResidue;
  let oilResidue;
  let gasVolume = 200;
  let oilVolume = 10;
  let ignition = false;
  let ready = false;
  let engineStatus = true;
  /**
	 * Check gas amount after riding.
	 * @param distance {number} - Riding distance.
	 * @return {number} - Gas amount.
	 */
  function gasCheck(distance) {
    if (gasBalance <= 0) {
      return 0;
    }

    let gasForRide = Math.round(distance * gasConsumption);

    return gasBalance - gasForRide;
  }

  function oilChek(distance){
    if (oilBalance <= 0) {
      return 0;
    }
    let oilForRide = (Math.round((distance * oilConsumption) * 100) / 100);
    if (oilVolume >= oilBalance) {
      showMessage('Oil must be changed', 'warning');
    }
    return oilBalance - oilForRide;
  }

  /**
	 * Show message for a user in the console.
	 * @param message {string} - Text message for user.
	 * @param type {string} - Type of the message (error, warning, log). log by default.
	 */
  function showMessage(message, type) {
    let messageText = message ? message : 'Error in program. Please call - 066 083 07 06';

    switch (type) {
      case 'error':
        console.error(messageText);
        break;
      case 'warning':
        console.warn(messageText);
        break;
      case 'log':
        console.log(messageText);
        break;
      default:
        console.log(messageText);
        break;
    }
  }

  /**
	 * Check car for ride.
	 * @param distance {number} - Ride distance.
	 */
  function checkRide(distance) {
    gasResidue = gasCheck(distance);
    oilResidue = oilChek(distance);
  }

  /**
	 * Check value to number.
	 * @param n {void} - value.
	 * @return {boolean} - Is number.
	 */
  function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }

  /**
	 * Public methods object.
	 */
  return {
    /**
		 * Start car.
		 */
    start: function() {
      ignition = true;

      if(engineStatus === false){
        showMessage('Engine is broken, Call in service', 'error');
        ready = false;
        return;
      }

      if (gasBalance <= 0) {
        showMessage('You don\'t have gas. Please refuel the car.', 'error');
        ready = false;
        return;
      } 
                 
      ready = true;

      showMessage('Ingition', 'log');
    },

    callService:function(){
      engineStatus = true;
      oilBalance = 100;
      showMessage('Your car is repaired', 'log');
    },

    /**
		 * Riding function.
		 * @param distance {number} - Riding distance.
		 */
    riding: function(distance, speedRide) {
      if (!isNumeric(distance)) {
        showMessage('Wrong distance', 'error');
        return;
      }

      if (!ignition) {
        showMessage('You need start car', 'error');
        return;
      }

      if (!ready) {
        ignition = false;
        showMessage('You need gas station', 'error');
        return;
      }
              
      checkRide(distance);

      let gasDriven = Math.round(gasBalance * gasConsumption * 100);

      if (gasResidue <= 0) {
        let distanceLeft = Math.round(distance - gasDriven);

        gasBalance = 0;
        let neddedGas = distanceLeft * gasConsumption;

        showMessage('Gas over. You have driven - ' + gasDriven + '. You need ' + + neddedGas + ' liters. ' + distanceLeft + 'km left', 'warning');
      }

      if (gasResidue > 0) {
        gasBalance = gasResidue;
        showMessage('You arrived. Gas balance - ' + gasResidue, 'log');
      }   

      if(maxSpeed < speedRide){
        showMessage('You cannot speed faster than the speed limit. Maximum vehicle speed ' + maxSpeed + ' km/h', 'error');
        return;
      } else {
        let timeLeft = distance / speedRide;
        showMessage('Ride time ' + Math.round(timeLeft * 60) + ' minutes', 'log');
      }
      
      if (oilResidue <= 10) {
        oilBalance = 0;
        
        engineStatus = false;
        ignition = false;
        showMessage('Oil over. The engine suddenly stalled', 'warning');
      }

      if (oilResidue > 0) {
        oilBalance = oilResidue;
        showMessage('Oil condition - ' + (Math.round(oilResidue * 100) / 100) + '%', 'log');
      }
    },

    /**
		 * Check gas function.
		 */
    checkGas: function() {
      showMessage('Gas - ' + gasBalance, 'log');
    },

     /**
		 * Check oil function.
		 */
    checkOil: function() {
      showMessage('Oil - ' + (Math.round(oilBalance * 100) / 100) + '%', 'log');
    },

    /**
		 * Refueling function.
		 * @param gas
		 */
    refueling: function(gas) {
      if (!isNumeric(gas)) {
        showMessage('Wrong gas amount', 'error');
        return;
      }

      if (gasVolume === gasBalance) {
        showMessage('Gasoline tank is full', 'warning');
      } else if (gasVolume < gasBalance + gas) {
        gasBalance = 200;
        let excess = Math.round(gas - gasBalance);
        showMessage('Gasoline tank is full. Excess - ' + excess, 'log');
      } else {
        gasBalance = Math.round(gasBalance + gas);
        showMessage('Gas balance - ' + gasBalance, 'log');
      }
    },
    
    //  Relpace oil function.
    replacedOil: function() {
      if (oilVolume > oilBalance) {
        oilBalance = 100;
        showMessage('Oil replaced!', 'log');
      } 
    }
  };
}
