
# Car

Game for learning JavaScript course.

## Getting Started
##### To start writing in the browser console
-------------
#### Create car
`var car = Car(5,5,180);`  Car(gasoline consumption, oil consumption, max car speed);
#### Ignition
`car.start();` Start engine
#### Ride
`car.riding(200, 80);` car.riding(distance, speed);
#### Check gas
`car.checkGas();` Fuel residue
#### Check oil
`car.checkOil();` Oil condition
#### Refueling gas
`car.refueling(100);` car.refueling(gas amount);
#### Replaced oil
`car.replacedOil();` Replaced oil
#### Call service
`car.callService();` Сall the service if the car is broken
